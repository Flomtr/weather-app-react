import React from "react";

const Header = () => (
    <div className="header">
        <h1 className="header__title">
            Easy Weather
        </h1>
    </div>
)

export default Header