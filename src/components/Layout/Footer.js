import React from "react";

const Footer = () => {
    return (
        <div className="footer">
            <p>Intégration et Développement -
                 <a rel="noopener noreferrer" target="_blank"
                   href="https://florian-mottura.fr">
                     Florian Mottura
                </a>
            </p>
        </div>
    )
}

export default Footer