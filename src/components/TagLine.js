import React from "react";

const TagLine = () => {
    return (
        <div className="tagline">
            <p>Entrez le nom d'une ville !</p>
        </div>
    )
}

export default TagLine