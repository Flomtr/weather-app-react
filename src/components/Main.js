import React, {useState} from "react";
import axios from "axios";
import Context from "../Context";
import Header from "./Layout/Header";
import TagLine from "./TagLine";
import Content from "./Layout/Content";
import WeatherSearch from "./WeatherSearch";
import WeatherData from "./WeatherData";
import Error from "./Error";
import DateTime from "./DateTime";
import Footer from "./Layout/Footer";

const Main = () => {

    const [weather, setWeather] = useState()
    const [city, setCity] = useState()
    const [error, setError] = useState()

    const api_call = async e => {
        e.preventDefault()
        const location = e.target.elements.location.value
        if (!location) return setError("Veuillez renseigner une ville"), setWeather(null)
        const API_KEY = "9f4ae4e45315166d8ddc69d092780d52"
        const url = `https://api.openweathermap.org/data/2.5/weather?q=${location}&appid=${API_KEY}&units=metric`
        const request = axios.get(url)
        const response = await request
        setWeather(response.data.main)
        setCity(response.data.name)
        setError(null)
    }

    console.log(weather)

    return (
        <div className="main">
            <Header/>
            <Content>
                <DateTime/>
                <TagLine/>
                <Context.Provider value={{api_call, weather, city}}>
                    <WeatherSearch/>
                    {weather && <WeatherData/>}
                    {error && <Error error={error}/>}
                </Context.Provider>
                <Footer/>
            </Content>
        </div>
    )
}

export default Main