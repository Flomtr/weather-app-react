import React, {useContext} from "react";
import Context from "../Context";

const WeatherData = () => {

    const {weather, city} = useContext(Context)
    const {temp, humidity, pressure} = weather
    return (
        <div className="weather-data">
            <p className="weather__tagline">
                Informations Méteo pour
                <span className="weather-data__city"> {city}</span>
            </p>
            <div className="weather-data__box">
                <div className="weather-data__content">
                    <span className="weather-data__property">
                        <p className="weather-data__title">Température</p>
                        <p className="weather-data__value">{Math.round(temp)}<span>&#176;</span></p>
                    </span>
                </div>
                <div className="weather-data__content">
                    <span className="weather-data__property">
                        <p className="weather-data__title">Humidité</p>
                        <p className="weather-data__value">{humidity} %</p>
                    </span>
                </div>
                <div className="weather-data__content">
                    <span className="weather-data__property">
                        <p className="weather-data__title">Pression</p>
                        <p className="weather-data__value">{pressure}</p>
                    </span>
                </div>
            </div>
        </div>
    )
}

export default WeatherData