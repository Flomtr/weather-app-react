import React from "react";

const DateTime = () => {
    const date = new Date()

    return (
        <div className="date-time">
            Le {`${date.toLocaleDateString()} à ${date.toLocaleTimeString('fr-FR', {
            hour: '2-digit',
            minute: '2-digit'
        })}`}
        </div>
    )
}

export default DateTime